package com.example.thecarddb.ui.main;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.bumptech.glide.Glide;
import com.example.thecarddb.R;

import java.util.List;

public class CardsAdapter extends ArrayAdapter<Cards> {
    public CardsAdapter(Context context, int resource, List<Cards> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Obtenim l'objecte en la possició corresponent
        Cards cards = getItem(position);

        // Mirem a veure si la View s'està reusant, si no es així "inflem" la View
        // https://github.com/codepath/android_guides/wiki/Using-an-ArrayAdapter-with-ListView#row-view-recycling
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.lv_cards_row, parent, false);
        }

        // Unim el codi en les Views del Layout
        TextView tvCards = convertView.findViewById(R.id.tvCards);
        TextView tvRarity = convertView.findViewById(R.id.tvRarity);
        ImageView ivCardImage = convertView.findViewById(R.id.ivCardImage);

        // Fiquem les dades dels objectes (provinents del JSON) en el layout
        tvCards.setText(cards.getName());
        tvRarity.setText(cards.getRarity());
        Glide.with(getContext()).load(
                cards.getImageUrl()
        ).into(ivCardImage);

        // Retornem la View replena per a mostrarla
        return convertView;
    }
}