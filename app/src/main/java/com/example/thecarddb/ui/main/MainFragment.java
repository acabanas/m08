package com.example.thecarddb.ui.main;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.thecarddb.Detail;
import com.example.thecarddb.R;

import org.jetbrains.annotations.NotNull;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;
    private CardsAdapter adapter;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment, container, false);

        ListView lvCards = view.findViewById(R.id.lvCards);
        ArrayList <Cards> items = new ArrayList<>();

        adapter = new CardsAdapter(
                getContext(),
                R.layout.lv_cards_row,
                items
        );

        lvCards.setAdapter(adapter);
        lvCards.setOnItemClickListener((adapter, fragment, i, l) -> {
            Cards cards = (Cards) adapter.getItemAtPosition(i);
            if (!esTablet()) {
                Intent intent = new Intent(getContext(), Detail.class);
                intent.putExtra("card", cards);
                startActivity(intent);
            } else {
                sharedModel.select(cards);
            }
        });

        model = ViewModelProviders.of(this).get(MainViewModel.class);
        mViewModel.getCards().observe(this, cards -> {
            adapter.clear();
            adapter.addAll(cards);
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        // TODO: Use the ViewModel
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Cards>> {
        @Override
        protected ArrayList<Cards> doInBackground(Void... voids) {
            CardDBAPI api = new CardDBAPI();
            ArrayList<Cards> result = api.getCards();
            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<Cards> cards) {
            adapter.clear();
            for (Cards card: cards) {
                adapter.add(card);
            }
        }
    }

    public void onCreateOptionsMenu(@NotNull Menu menu, @NotNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_cards_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            refresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    private void refresh() {
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }
}