package com.example.thecarddb.ui.main;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainViewModel extends ViewModel {
    // TODO: Implement the ViewModel

    private final Application app;
    private final AppDatabase appDatabase;
    private final CardDAO movieDao;
    private LiveData<List<Cards>> movies;

    public MainViewModel(Application application) {
        super();

        this.app = application;
        this.appDatabase = AppDatabase.getDatabase(
                this.getApplication());
        this.movieDao = appDatabase.getCardDao();
    }

    public LiveData<List<Cards>> getCards() {
        return movieDao.getCards();
    }


    public void reload() {
        // do async operation to fetch users
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {

           CardDBAPI api = new CardDBAPI();
           ArrayList<Cards> result = api.getCards();

            movieDao.deleteCards();
            movieDao.addCards(result);

            return null;
        }

    }
}