package com.example.thecarddb.ui.main;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class Cards implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private String type;
    private String rarity;
    private String imageUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "Card{" +
                "Name='" + name + '\'' +
                ", Type='" + type + '\'' +
                ", Rarity='" + rarity + '\'' +
                ", Image='" + imageUrl + '\'' +
                '}';
    }
}