package com.example.thecarddb.ui.main;

import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.thecarddb.R;



public class DetailFragment extends Fragment {

    private View view;
    private ImageView ivCardImage;
    private TextView tvCards;
    private TextView tvRarity;

    private MainViewModelDetail mViewModel;

    public static DetailFragment newInstance() {
        return new DetailFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.main_fragmentdetail, container, false);

        Intent i = getActivity().getIntent();

        if (i != null) {
            Cards card = (Cards) i.getSerializableExtra("card");

            if (card != null) {
                updateUi(card);
            }
        }

        return view;
    }

    private void updateUi(Cards card) {
        Log.e(null, card.toString());

        ivCardImage = view.findViewById(R.id.ivCardImage1);
        tvCards = view.findViewById(R.id.tvCards1);
        tvRarity = view.findViewById(R.id.tvRarity1);

        tvCards.setText(card.getName());
        tvRarity.setText(card.getRarity());
        Glide.with(getContext()).load(
                card.getImageUrl()
        ).into(ivCardImage);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(MainViewModelDetail.class);
        // TODO: Use the ViewModel
    }
}