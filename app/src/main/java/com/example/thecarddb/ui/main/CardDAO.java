package com.example.thecarddb.ui.main;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface CardDAO {
    @Query("select * from cards")
    LiveData<List<Cards>> getCards();

    @Insert
    void addCards(Cards cards);

    @Insert
    void addCards(List<Cards> cards);

    @Delete
    void deleteCards(Cards cards);

    @Query("DELETE FROM cards")
    void deleteCards();
}